# Toast and Tech website, in Gatsby

It's pretty much the same site, just done in gatsby, cos that's cool.

## Deploying ##

    $ yarn run build
    $ yarn run preview # (optional)
    $ yarn run deploy

That should ship it out to `http://toastand.tech` if you're me.

## Developing ##

    $ yarn start

Follow the guide in the original [Gatsby README](./gatsby-README.md).
