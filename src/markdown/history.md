---
title: "History"
sectionId: history
---

Toast and Tech began as a request by some folks going through some introductory classes in HTML, CSS, and JavaScript to have an extra time to do lab work together, not necessary specific instruction, but to be able to ask and answer questions, and help each other through the class work.

We attended a couple times at the <span class="contrast-link">[Canteen coffee house and toast bar](https://www.canteen3255.com/)</span>, in the Lyn-Lake area. Some of us enjoyed the time and the place well enough to jusy keep going as an unofficial study group. Information was usually passed via twitter, facebook, and slack, and there were no signups, agendas, etc., just a drop in when you want sort of thing.

Attendance kept going up, and we decided we should have a name. There were a few other meetings called "Code and Coffee" and we wanted to differentiate ourselves from that, so Tamara came up with the name "Toast and Tech" to keep it in the alliterative form, and reflect our relationship with the Canteen.

In late 2018, the parent group we were affiliated with, Girl Develop It, as part of the Minneapolis chapter, was imploading, and we stopped using that meetup account for signups. The issues with GDI are <span class="contrast-link">[ongoing](http://an-open-letter-to-gdi-board.com/ "An Open Letter to the Board of Girl Develop It")</span>.

We're starting a new Toast &amp; Tech in St. Paul for folks who don't or can't travel across the river and 35W from the east side of the metro. There's also a group meeting in Minneapolis called **CodeJam** on Fridays.

St. Paul meetups have been happening at Tamara&apos;s house occasionally on Monday evenings.
