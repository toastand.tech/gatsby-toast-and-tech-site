---
title: Tenets
sectionId: tenets
---
Not rules, bylaws, or articles of incorporation, these are some beliefs and values of toast and tech:

- make EVERYONE welcome, enthusiastically
- make ALL questions welcome, enthusiastically
- everyone is learning, everyone is teaching
- there is no single path, there are as many paths as people attending, as many paths as needed
- you don't need a project or tech work to attend, just come
- you don't need any experience to attend, just come
- support everyone who shows up
- Make it safe to learn, safe to ask questions, safe to show up

And possibly the most important of all:

- Toast and Tech can happen *anywhere*, be run by *anyone*; all it takes is letting folks know about it, and showing up to hold the space.
