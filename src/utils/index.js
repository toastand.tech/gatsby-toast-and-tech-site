export function inTheFuture(eventDate, duration = 3, useDate = null) {
  const nowDate = useDate || new Date()
  let endDate = new Date(eventDate.valueOf())
  endDate.setHours(eventDate.getHours() + duration)
  let isItInTheFuture = endDate.valueOf() > nowDate.valueOf()
  console.log({
    in: "inTheFuture",
    eventDate,
    nowDate,
    endDate,
    isItInTheFuture,
  })
  return isItInTheFuture
}

export function getNextDate(dateList) {
  dateList.find(item => inTheFuture(item))
}
