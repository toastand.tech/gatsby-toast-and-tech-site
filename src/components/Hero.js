import React from "react"
import { Section, SectionBody } from "./section"
import heroImage from "../images/cup-of-chai-and-a-laptop-800.png"
import HeroImageCredit from "./chaiAndLaptopImageCredit"
import styled from "styled-components"

export default props => (
  <Section>
    <SectionBody>
      <HeroImg
        src={heroImage}
        alt="With a laptop and a cup of coffee I can change the world"
      />
      <HeroImageCredit />
    </SectionBody>
  </Section>
)

const HeroImg = styled.img`
  display: block;
  width: 90%;
  margin: 0 auto;
`
