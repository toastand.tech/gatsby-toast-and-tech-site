import React from "react"
import styled from "styled-components"
import { StyledExtLink } from "./global"
import poster from "../images/ToastAndTechPoster.jpg"

export default props => (
  <SidebarWrapper>
    <SidebarNav>
      <SidebarItem>
        <StyledExtLink href="#announcements-section">
          Announcements
        </StyledExtLink>
      </SidebarItem>
      <SidebarItem>
        <StyledExtLink href="#tenets-section">Tenets</StyledExtLink>
      </SidebarItem>
      <SidebarItem>
        <StyledExtLink href="#history-section">History</StyledExtLink>
      </SidebarItem>
      <SidebarItem>
        <StyledExtLink href="#contact-section">Contact</StyledExtLink>
      </SidebarItem>
      <SidebarItem>
        <StyledExtLink href="#future-session-section">
          Future Sessions
        </StyledExtLink>
      </SidebarItem>
    </SidebarNav>
    <SidebarImg src={poster} alt="Toast and Tech Poster" />
    <Source>
      <StyledExtLink
        href="https://gitlab.com/toastand.tech/gatsby-toast-and-tech-site"
        title="View source on Gitlab"
        target="_blank"
        rel="noopener noreferrer nofollow noindex"
      >
        Source
      </StyledExtLink>
    </Source>
  </SidebarWrapper>
)

const SidebarWrapper = styled.div`
  font-family: ${props => props.theme.fonts.family};
  font-size: ${props => props.theme.fonts.size};
  color: ${props => props.theme.colors.textColor};
  background: ${props => props.theme.colors.brandSecondary};
  border: 1px solid ${props => props.theme.colors.brandHighlight};
  border-radius: 15px;
  margin: 0;
  padding: 10px;
  @media (min-width: 450px) {
    // max-width: 200px;
    text-align: right;
    display: flex;
    flex-direction: column;
    align-items: stretch;
    justify-content: space-between;
  }
`

const SidebarNav = styled.ul`
  list-style: none;
  margin: 0;
  padding: 0;
`
const SidebarItem = styled.li`
  font-size: 1.1em;
  margin: 0;
  padding: 0;
`
const Source = styled.div`
  margin: 0 0 20px;
  font-size: 85%;
`
const SidebarImg = styled.img`
  display: block;
  margin: 10px 0;
  padding: 2px;
  width: 100%;
  border-radius: 15px;
`
