/**
 * Layout component that queries for data
 * with Gatsby's useStaticQuery component
 *
 * See: https://www.gatsbyjs.org/docs/use-static-query/
 */

import React from "react"
import PropTypes from "prop-types"
import { useStaticQuery, graphql } from "gatsby"
import "./index.css"
import styled, { ThemeProvider } from "styled-components"

import Header from "./header"
import Sidebar from "./sidebar"
import theme from "./theme"

const Layout = ({ children }) => {
  const data = useStaticQuery(graphql`
    query SiteTitleQuery {
      site {
        siteMetadata {
          title
        }
      }
    }
  `)

  return (
    <ThemeProvider theme={theme}>
      <LayoutGrid>
        <Header siteTitle={data.site.siteMetadata.title} />
        <SidebarContent>
          <Sidebar />
        </SidebarContent>
        <MainContent>{children}</MainContent>
      </LayoutGrid>
    </ThemeProvider>
  )
}

Layout.propTypes = {
  children: PropTypes.node.isRequired,
}

export default Layout

const LayoutGrid = styled.div`
  display: grid;
  height: calc(100vh - 20px);
  margin: 0 auto;
  width: calc(100vw - 20px);
  grid-template-columns: 1fr;
  grid-template-areas: "header" "content" "sidebar";
  @media screen and (min-width: 500px) {
    grid-template-columns: 1fr 3fr;
    grid-template-areas: ". header" "sidebar content";
    grid-gap: 5px;
  }
  @media (min-width: 960px) {
    width: 960px;
  }
`

const MainContent = styled.article`
  grid-area: content;
  font-family: ${props => props.theme.fonts.family};
  font-size: ${props => props.theme.fonts.size};
  color: ${props => props.theme.colors.textColor};
  background: ${props => props.theme.colors.brandColor};
  border: 1px solid ${props => props.theme.colors.brandHighlight};
  border-radius: 15px;
  padding: 0 10px 1em;
`

const SidebarContent = styled.aside`
  grid-area: sidebar;
`
