import React from "react"
// import { graphql, useStaticQuery } from "gatsby"
import { Section, SectionHeading, SectionBody } from "./section"
import { ContrastExternalLink } from "./global"
import BackToTop from "./BackToTopLink"

// const query = graphql`
//   query MyQuery {
//     site {
//       siteMetadata {
//         contacts {
//           tamara {
//             name
//             twitter
//             slack {
//               handle
//               team
//             }
//           }
//         }
//       }
//     }
//   }
// `

export default () => {
  // const data = useStaticQuery(query)
  // const {
  //   site: {
  //     siteMetadata: {
  //       contacts: {
  //         tamara: {
  //           name = "Tamara Temple",
  //           twitter = "tamouse",
  //           slack: {
  //             handle = "tamouse",
  //             team = "wwcodetc",
  //             longTeam = "Women Who Code Twin Cities",
  //           },
  //         },
  //       },
  //     },
  //   },
  // } = data
  // const twitterLink = `https://twitter.com/${twitter}`,
  //   twitterText = `@${twitter}`,
  //   slackLink = `https://${team}.slack.com`

  return (
    <Section id="contact-section">
      <SectionHeading>Contact</SectionHeading>
      <SectionBody>
        <p>
          Contact me at{" "}
          <ContrastExternalLink href="mailto:tamara@toastand.tech">
            tamara@toastand.tech
          </ContrastExternalLink>
          .
        </p>
        <BackToTop />
      </SectionBody>
    </Section>
  )
}
