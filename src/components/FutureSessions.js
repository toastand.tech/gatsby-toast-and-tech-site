import React from "react"
import { format } from "date-fns"
import { graphql, useStaticQuery } from "gatsby"
import { Section, SectionBody, SectionHeading } from "./section"
import { CenteredText } from "./global"
import BackToTop from "./BackToTopLink"

// NOTE: What a freaking hack! You can't have an empty array in the data for the following
// query. If the collection is empty, it doesn't return an empty array for futureSessions,
// it doesn't even return a property for the siteMetadata object. What a stupid
// things. Down in the map over futureSessions, I have to toss an entry with an empty
// string, instead of being able to say "There are no future sessions". Gay. Hate it. It's
// wrong, gatsby.

const futureSessionsQuery = graphql`
  query FutureSessionsQuery {
    site {
      siteMetadata {
        futureSessions
      }
    }
  }
`

const FutureSessions = () => {
  const data = useStaticQuery(futureSessionsQuery)
  const {
    site: {
      siteMetadata: { futureSessions },
    },
  } = data

  return (
    <Section id={`future-session-section`}>
      <SectionHeading>{`Future Sessions`}</SectionHeading>
      <SectionBody>
        {futureSessions.length > 1 ? (
          futureSessions.map((session, index) => {
            if (session.length < 1) {
              return null
            }
            const sessionDate = new Date(session)
            return (
              <CenteredText key={`${session}-${index}`}>
                {format(sessionDate, "PPPP")}
              </CenteredText>
            )
          })
        ) : (
          <p>No future sessions scheduled yet.</p>
        )}
        <BackToTop />
      </SectionBody>
    </Section>
  )
}

export default FutureSessions
