import React from "react"
import { Section, SectionBody, SectionHeading } from "./section"
import BackToTop from "./BackToTopLink"

const Tenets = () => {
  return (
    <Section id={`tenets-section`}>
      <SectionHeading>{`Tenets`}</SectionHeading>
      <SectionBody>
        <p>
          Not rules, bylaws, or articles of incorporation, these are some
          beliefs and values of toast and tech:
        </p>
        <ul>
          <li>make EVERYONE welcome, enthusiastically</li>
          <li>make ALL questions welcome, enthusiastically</li>
          <li>everyone is learning, everyone is teaching</li>
          <li>
            there is no single path, there are as many paths as people
            attending, as many paths as needed
          </li>
          <li>you don’t need a project or tech work to attend, just come</li>
          <li>you don’t need any experience to attend, just come</li>
          <li>support everyone who shows up</li>
          <li>Make it safe to learn, safe to ask questions, safe to show up</li>
        </ul>
        <p>And possibly the most important of all:</p>
        <ul>
          <li>
            Toast and Tech can happen <em>anywhere</em>, be run by{" "}
            <em>anyone</em>; all it takes is letting folks know about it, and
            showing up to hold the space.
          </li>
        </ul>
        <BackToTop />
      </SectionBody>
    </Section>
  )
}

export default Tenets
