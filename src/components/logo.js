import React from "react"
import logo from "../images/kawaii-toast.png"
import styled, { keyframes } from "styled-components"

export const Logo = props => (
  <LogoImage src={logo} alt="Spinning kawaii toast logo" />
)

export default Logo

const rotate = keyframes`
  from {
    transform: rotate(0deg);
  }
  to {
    transform: rotate(360deg);
  }
`

const LogoImage = styled.img`
  width: 80px;
  height: 80px;
  margin-top: -5px;
  border-radius: 50%;
  @media (min-width: 450px) {
    animation: ${rotate} 4000ms linear infinite;
  }
`
