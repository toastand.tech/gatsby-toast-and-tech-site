import React from "react"
import { Section, SectionBody, SectionHeading } from "./section"
import BackToTop from "./BackToTopLink"
import "./contrast_link.css"

const History = () => {
  return (
    <Section id={`history-section`}>
      <SectionHeading>{`History`}</SectionHeading>
      <SectionBody>
        <p>
          Toast and Tech began as a request by some folks going through some
          introductory classes in HTML, CSS, and JavaScript to have an extra
          time to do lab work together, not necessary specific instruction, but
          to be able to ask and answer questions, and help each other through
          the class work.
        </p>

        <p>
          We attended a couple times at the{" "}
          <span className="contrast-link">
            <a href="https://www.canteen3255.com/">
              Canteen coffee house and toast bar
            </a>
          </span>
          , in the Lyn-Lake area. Some of us enjoyed the time and the place well
          enough to jusy keep going as an unofficial study group. Information
          was usually passed via twitter, facebook, and slack, and there were no
          signups, agendas, etc., just a drop in when you want sort of thing.
        </p>

        <p>
          Attendance kept going up, and we decided we should have a name. There
          were a few other meetings called “Code and Coffee” and we wanted to
          differentiate ourselves from that, so Tamara came up with the name
          “Toast and Tech” to keep it in the alliterative form, and reflect our
          relationship with the Canteen.
        </p>

        <p>
          In late 2018, the parent group we were affiliated with, Girl Develop
          It, as part of the Minneapolis chapter, was imploading, and we stopped
          using that meetup account for signups. The issues with GDI are{" "}
          <span className="contrast-link">
            <a
              href="http://an-open-letter-to-gdi-board.com/"
              title="An Open Letter to the Board of Girl Develop It"
            >
              ongoing
            </a>
          </span>
          .
        </p>

        <p>
          We’re starting a new Toast &amp; Tech in St. Paul for folks who don’t
          or can’t travel across the river and 35W from the east side of the
          metro. There’s also a group meeting in Minneapolis called{" "}
          <strong>CodeJam</strong> on Fridays.
        </p>

        <p>
          St. Paul meetups have been happening at Tamara's house occasionally on
          Monday evenings.
        </p>

        <h3>In these times of isolation</h3>
        <p>
          I am starting a virtual version of toast and tech, using Discord. See
          the announcement above for information on how to get involved.
        </p>
        <BackToTop />
      </SectionBody>
    </Section>
  )
}

export default History
