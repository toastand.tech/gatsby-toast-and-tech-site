import styled from "styled-components"
import { Link } from "gatsby"

export const StyledLink = styled(Link)`
  color: ${props => props.theme.colors.textColor};
  text-decoration: none;
  &:hover {
    text-decoration: underline;
  }
`
export const StyledExtLink = styled.a`
  color: ${props => props.theme.colors.textColor};
  text-decoration: none;
  &:hover {
    text-decoration: underline;
  }
`

export const ContrastLink = styled(Link)`
  color: ${props => props.theme.colors.highlightColor};
  text-decoration: dashed;
`

export const ContrastExternalLink = styled.a`
  color: ${props => props.theme.colors.highlightColor};
  text-decoration: dashed;
`

export const CenteredText = styled.div`
  text-align: center;
`

export const ResponsiveImg = styled.img`
  diplay: block;
  width: 95%;
`
