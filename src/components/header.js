import { Link } from "gatsby"
import PropTypes from "prop-types"
import React from "react"
import styled from "styled-components"
import Logo from "./logo"

const Header = ({ siteTitle, ...props }) => (
  <SiteHeader>
    <HeaderTitle>
      <HeaderLink to="/">{siteTitle}</HeaderLink>
    </HeaderTitle>
    <Logo />
  </SiteHeader>
)

Header.propTypes = {
  siteTitle: PropTypes.string,
}

Header.defaultProps = {
  siteTitle: ``,
}

export default Header

const SiteHeader = styled.header`
  grid-area: header;
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
  color: ${props => props.theme.colors.textColor};
  background-color: ${props => props.theme.colors.brandColor};
  border: 1px solid ${props => props.theme.colors.brandHighlight};
  border-radius: 15px;
  padding: 0 10px;
`

const HeaderTitle = styled.h1`
  font-family: ${props => props.theme.fonts.family};
`

const HeaderLink = styled(Link)`
  color: white;
  text-decoration: none;
`
