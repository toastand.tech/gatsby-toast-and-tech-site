import React from "react"
// import { graphql, useStaticQuery } from "gatsby"
// import { format } from "date-fns"

import { NarrowCenteredSection, SectionHeading, SectionBody } from "./section"
import {
  ContrastExternalLink,
  // CenteredText
} from "./global"
// import { inTheFuture } from "../utils"

// const query = graphql`
//   query AnnoucementQuery {
//     site {
//       siteMetadata {
//         nextSession {
//           date
//           location {
//             label
//             url
//           }
//           eventBriteUrl
//         }
//       }
//     }
//   }
// `

export default () => {
  // const data = useStaticQuery(query)
  // const {
  //   site: {
  //     siteMetadata: { nextSession },
  //   },
  // } = data

  // const eventDate = new Date(nextSession.date),
  //   location = nextSession.location,
  //   eventBriteUrl = nextSession.eventBriteUrl

  // NOTE: there's a bug in the format method for formatting the time: `format(eventDate. "h:mmb")` that shows noon on some mobile browsers. Since it's always 6pm, i just hard coded it.
  return (
    <NarrowCenteredSection id="announcements-section">
      <SectionHeading>We&apos;re going virtual!</SectionHeading>
      <SectionBody>
        <h2>
          Toast and Tech is moving to a virtual setting during this time of
          isolation and lock ins.
        </h2>
        <div>
          I have set up a{" "}
          <ContrastExternalLink href="https://discord.com">
            Discord
          </ContrastExternalLink>{" "}
          server that includes chat rooms dedicated to specific topics, voice
          and video rooms that can be reserved for specific study group
          sessions, with plans to start hosting weeknight sessions again soon.
        </div>
        <div>
          Contact me at{" "}
          <ContrastExternalLink href="mailto:tamara@toastand.tech">
            tamara@toastand.tech
          </ContrastExternalLink>{" "}
          to get an invite code. Please note if you already have a discord user,
          contact me directly at <code>tamouse#3242</code> to get an invite.
        </div>
      </SectionBody>
    </NarrowCenteredSection>
  )
}
