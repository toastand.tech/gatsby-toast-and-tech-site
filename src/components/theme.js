export default {
  colors: {
    brandColor: `hsla(25, 65%, 50%, 1)`,
    brandSecondary: `hsla(25, 95%, 50%, 1)`,
    brandHighlight: `hsla(55, 85%, 100%, 1)`,
    textColor: `#DEDEDE`,
    highlightColor: `#FFF`,
  },
  fonts: {
    family: "sans-serif",
    size: "1.1em",
  },
}
