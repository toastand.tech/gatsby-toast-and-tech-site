import React from "react"
import styled from "styled-components"

export default () => (
  <CenteredDiv>
    <ContrastLink href="#top">Back to top</ContrastLink>
  </CenteredDiv>
)

export const CenteredDiv = styled.div`
  text-align: center;
`
export const ContrastLink = styled.a`
  color: ${props => props.theme.colors.highlightColor};
  text-decoration: dashed;
  font-size: 0.8em;
`
