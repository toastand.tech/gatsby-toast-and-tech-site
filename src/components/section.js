import styled from "styled-components"
import { Link } from "gatsby"

export const Section = styled.section`
  margin-top: 1em;
  border-top: 2px solid ${props => props.theme.colors.brandHighlight};
`
export const SectionHeading = styled.h3`
  color: ${props => props.theme.colors.highlightColor};
  padding-top: 5px;
`
export const SectionBody = styled.div``
export const SectionLink = styled(Link)`
  color: ${props => props.theme.colors.textColor};
  text-decoration: none;
  &:hover {
    text-decoration: underline;
  }
`

export const NarrowCenteredSection = styled.section`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  width: 60%;
  margin: 1em auto;
`
