import React from "react"
import Layout from "../components/layout"
import SEO from "../components/seo"
import Contact from "../components/contact.js"
import { ContrastLink, CenteredText, ResponsiveImg } from "../components/global"
import smokingToaster from "../images/smoking-toaster.jpg"

const NotFoundPage = () => (
  <Layout>
    <SEO title="404: Not found" />
    <CenteredText>
      <p>
        <ResponsiveImg src={smokingToaster} alt="smoking toaster" />
      </p>
      <h1>NOT FOUND</h1>
      <p>You just hit a route that doesn&#39;t exist... the sadness.</p>
      <p>
        Return to <ContrastLink to="/">Home</ContrastLink>
      </p>
    </CenteredText>
    <Contact />
  </Layout>
)

export default NotFoundPage
