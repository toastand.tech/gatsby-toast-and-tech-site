import React from "react"
import Layout from "../components/layout"
import SEO from "../components/seo"
import Contact from "../components/contact"

import Announcement from "../components/Announcement"
import Hero from "../components/Hero"
import Tenets from "../components/Tenets"
import History from "../components/History"
import FutureSessions from "../components/FutureSessions"

export default () => {
  return (
    <Layout id="top">
      <SEO />
      <Hero />
      <Announcement />
      <Tenets />
      <History />
      <FutureSessions />
      <Contact />
    </Layout>
  )
}
