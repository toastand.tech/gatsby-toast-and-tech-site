module.exports = {
  siteMetadata: {
    title: `Toast and Tech`,
    description: `Gentle, supportive, healthy software development learning salons.`,
    author: `@tamouse`,
    nextSession: {
      date: "2020-02-17T18:00:00-06:00",
      location: {
        label: "1585 Dodd Road, Mendota Heights",
        url: "https://goo.gl/maps/CobuLiC2eCsoVo1n6",
      },
      eventBriteUrl: "",
    },
    // NOTE: the stupidity which is gatsby's data layer. If this array is empty, or
    // contains a null entry, the property is skipped completely. The kludge hack is to
    // put an empty string in the collection, and write the code for display to assume
    // there's always a dummy entry of an empty string in the arryy.
    futureSessions: [""],
    contacts: {
      tamara: {
        name: "Tamara Temple",
        twitter: "tamouse",
        slack: {
          team: "wwcodetc",
          longTeam: "Women Who Code Twin Cities",
          handle: "tamouse",
        },
      },
    },
  },
  plugins: [
    `gatsby-plugin-react-helmet`,
    `gatsby-transformer-remark`,
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `images`,
        path: `${__dirname}/src/images`,
      },
    },
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `markdown`,
        path: `${__dirname}/src/markdown`,
      },
    },
    `gatsby-transformer-sharp`,
    `gatsby-plugin-sharp`,
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `gatsby-starter-default`,
        short_name: `starter`,
        start_url: `/`,
        background_color: `#663399`,
        theme_color: `#663399`,
        display: `minimal-ui`,
        icon: `src/images/gatsby-icon.png`, // This path is relative to the root of the site.
      },
    },
    `gatsby-plugin-styled-components`,
    // this (optional) plugin enables Progressive Web App + Offline functionality
    // To learn more, visit: https://gatsby.dev/offline
    // `gatsby-plugin-offline`,
  ],
}
